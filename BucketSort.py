def bucket_sort(n, array):
    # 1) create n empty buckets
    bucket = []
    for i in range(n):
        bucket.append([])

    # 2) Put array elements in different buckets
    for i in range(n):
        bi = int(n * array[i])  # bucket index
        bucket[bi].append(array[i])

    # 3) sort individual buckets
    for i in range(n):
        bucket[i].sort()  # Timsort, derived from m.s. and i.s.

    # 4) Concatenate buckets
    index = 0
    for i in range(n):
        for j in range(len(bucket[i])):
            array[index] = bucket[i][j]
            index += 1
    return array